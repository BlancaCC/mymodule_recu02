# Class: myModule
# ===========================
#
# Full description of class myModule here.
# Examples
# --------
#
# @example
#    class { 'myModule':
#      servers => [ 'pool.ntp.org', 'ntp.local.company.com'16 ],
#    }
#

class mymodule {

include mymodule::shared
include mymodule::mysql_config
include mymodule::apache_config

}